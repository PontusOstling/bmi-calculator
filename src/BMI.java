import java.util.Scanner;


public class BMI {

	public static void main(String[] args) {
		//Variables
		
		Scanner input = new Scanner(System.in);
		double height = 0;
		double weight = 0;
		double result = 0;
		String goAgain = "";
		
		//Takes the user input and calculates.
		
		do {
			System.out.println("Welcome to the BMI calculator! Please enter your height: ");
			height = input.nextInt();
			System.out.println("Please enter your weight: ");
			weight = input.nextInt();
			
			result = weight / (height * height) *10000;
			
			//Takes the results and prints them out.
			
			if(result < 18.5){
				System.out.printf("Your BMI is %.2f. ", result);
				System.out.println("You're underweight. ");
			}
			else if(result >18.5 && result <24.9){
				System.out.printf("Your BMI is %.2f. ", result);
				System.out.println("You're normal. ");
			}
			else if(result >25 && result <29.9){
				System.out.printf("Your BMI is %.2f. ", result);
				System.out.println("You're overweight. ");
			}
			else{
				System.out.printf("Your BMI is %.2f. ", result);
				System.out.println("You're obese. ");
			}
			// Asks the user if he wants to calculate again.
			
			System.out.println("Do you want to calculate again? (y/n)");
			goAgain = input.next();
			
		} while(goAgain.equalsIgnoreCase("y"));
		
		input.close();

	}

}
